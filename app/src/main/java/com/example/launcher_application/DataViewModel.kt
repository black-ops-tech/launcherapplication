package com.example.launcher_application

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.launcher_application.adapters.ListFragmentAdapter
import java.util.*

class DataViewModel : ViewModel() {

    val list: MutableLiveData<MutableList<PackageInfo>> = MutableLiveData()

    fun setList(listOf: MutableList<PackageInfo>) {
        list.value = listOf
    }

    fun getList(): MutableList<PackageInfo>? {
        return list.value
    }


    fun modifyToAscendingOrder(
        list: MutableList<PackageInfo>,
        packageManager: PackageManager?
    ): MutableList<PackageInfo> {
        println("\nRESULT IN VIEW MODEL!! : " + list + "\n")
        var listModified: MutableList<String> = mutableListOf()
        var sourceDataListMod: MutableList<PackageInfo> = mutableListOf()
        for (i in list.indices) {
            listModified.add(list.get(i).applicationInfo?.loadLabel(packageManager!!).toString())
        }
        println("n\nRESULT LIST A: ${listModified}\n")
        listModified.sortBy { it }
        for (j in listModified) {
            for (k in list) {
                if (j.equals(k.applicationInfo.loadLabel(packageManager!!))) {
                    sourceDataListMod.add(k)
                }
            }
        }
        println("n\nRESULT LIST C: ${sourceDataListMod}\n")
        return sourceDataListMod
    }

    fun filter(
        text: String,
        installedListData: MutableList<PackageInfo>,
        context: Context
    ): ListFragmentAdapter {
        var filteredList: MutableList<PackageInfo> = mutableListOf()
        for (item in installedListData) {
            if (item.applicationInfo.loadLabel(context.packageManager).toString()
                    .toLowerCase(Locale.ROOT).contains(text.toLowerCase(Locale.ROOT))
            ) {
                filteredList.add(item)
            }
        }
        if (filteredList.isEmpty()) {
            println("NO DATA ATTACHED TO THIS LETTER!")
            return ListFragmentAdapter(context, installedListData, context.packageManager)
        } else {
            setList(filteredList)
            return ListFragmentAdapter(context, filteredList, context.packageManager)
        }
    }

    fun getSourceData(
        context: Context?,
        installedListData: MutableList<PackageInfo>
    ) {
        var listForsrc: MutableList<PackageInfo> = mutableListOf()
        listForsrc =
            context?.getPackageManager()?.getInstalledPackages(PackageManager.GET_META_DATA)!!
        for (i in listForsrc.indices) {
            val packageInfo = listForsrc[i]
            if (packageInfo.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM == 0) {
                installedListData.add(listForsrc.get(i))
            }
        }
        setList(
            modifyToAscendingOrder(
                installedListData,
                context?.packageManager
            )
        )
    }
}
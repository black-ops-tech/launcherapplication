package com.example.launcher_application.fragments

import android.content.pm.PackageInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.launcher_application.DataViewModel
import com.example.launcher_application.adapters.ListFragmentAdapter
import com.example.launcher_application.databinding.FragmentListBinding
import kotlinx.coroutines.runBlocking


class ListFragment : Fragment() {

    lateinit var binding: FragmentListBinding
    lateinit var viewModel: DataViewModel

    //    private searchView: SearchView

    var installedListData: MutableList<PackageInfo> = mutableListOf()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflateing layout for this fragment
        binding = FragmentListBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(requireActivity()).get(DataViewModel::class.java)
        viewModel.getSourceData(context, installedListData)
        return binding.root
    }


    private fun setRecyclerView() {
        binding.appRecyclerView.let {
            it.layoutManager = LinearLayoutManager(requireView().context)
            it.setHasFixedSize(true)
            it.adapter = ListFragmentAdapter(
                context,
                viewModel.getList()!!.toMutableList(),
                context?.packageManager
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        runBlocking {
            setRecyclerView()
        }
        binding.searchViewId.clearFocus()
        binding.searchViewId.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(msg: String): Boolean {
                binding.appRecyclerView.adapter =
                    viewModel.filter(msg, installedListData, requireContext())
                return false
            }
        })
    }
}
package com.example.launcher_application.adapters

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.launcher_application.R
import kotlinx.android.synthetic.main.app_info_layout.view.*


class ListFragmentAdapter(
    context: Context?,
    list: MutableList<PackageInfo>,
    packageManager: PackageManager?
) :
    RecyclerView.Adapter<ListFragmentAdapter.MyViewHolder>() {

    var listAp: MutableList<PackageInfo> = list
    var pm: PackageManager? = packageManager
    var ctx: Context? = context

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(
            context: Context?,
            position: Int,
            listAp: MutableList<PackageInfo>,
            pm: PackageManager?
        ) {
            itemView.app_name.text = listAp.get(position).applicationInfo.loadLabel(pm!!).toString()
            Glide.with(context!!)
                .load(listAp.get(position).applicationInfo.loadIcon(pm))
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_launcher_background)
                .into(itemView.app_image);
            itemView.app_pkg_name.text = listAp.get(position).packageName.toString()
            itemView.app_vrsn_code.text = listAp.get(position).versionCode.toString()
            itemView.app_vrsn_name.text = listAp.get(position).versionName.toString()
            itemView.setOnClickListener {
                println(
                    "YOOOOO!!${listAp.get(position).applicationInfo.loadLabel(pm)}\n" +
                            "package: ${listAp.get(position).applicationInfo.packageName}\n"
                )

                val launchIntent: Intent? =
                    context.getPackageManager()
                        .getLaunchIntentForPackage(listAp.get(position).applicationInfo.packageName)
                if (launchIntent != null) {
                    context.startActivity(launchIntent)
                } else {
                    Toast.makeText(
                        context,
                        "There is no package available in android",
                        Toast.LENGTH_LONG
                    ).show()
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.app_info_layout, parent, false)
        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(ctx, position, listAp, pm)
    }

    override fun getItemCount(): Int {
        return listAp.size
    }
}